import Link from "next/link";
import React, { ReactElement } from "react";
import DependeciesWrapper from "src/utils/DependeciesWrapper";
import Image from "next/image";
import { NextPage } from "next";
import { signOut } from "next-auth/react";
import { useRouter } from "next/router";

const getAdminLayout = (page: NextPage) => {
    const navLinks = [
        {
            icon: <img src="/icons/nav_inbox.svg" alt="bookings" />,
            label: "Записи",
            to: "/dashboard/bookings/613588dcc5a499f383433766",
        },
    ];

    return (
        <div className="bg-light-pink min-h-screen min-w-full container mx-auto">
            <section className="grid grid-cols-12 gap-4">
                <aside className="col-start-1 col-end-3 flex flex-col items-start py-8">
                    <div className="mb-12">
                        <Link href="/">
                            <Image
                                width="200"
                                height="100"
                                className=" cursor-pointer"
                                src="/icons/lucem-logo.svg"
                            />
                        </Link>
                    </div>
                    <ul className=" w-full flex flex-col justify-start items-start">
                        {navLinks.map((el) => (
                            <Link href={`${el.to}`}>
                                <li
                                    className={`flex justify-start items-center hover:bg-white text-dark-grey hover:text-purple-500 w-full h-full p-2 rounded-md`}
                                >
                                    <div className="flex items-center justify-start  font-medium cursor-pointer text-lg">
                                        <div className="mr-2">{el.icon}</div>
                                        <span className="flex-1">
                                            {el.label}
                                        </span>
                                    </div>
                                </li>
                            </Link>
                        ))}
                        <Link href="/">
                            <p className="text-dark-grey font-medium cursor-pointer text-lg mt-14 text-center">
                                Сайт клиники
                            </p>
                        </Link>
                        <button
                            onClick={() => {
                                signOut();
                            }}
                            className=" text-red-500 font-medium cursor-pointer text-lg mt-14 text-center"
                        >
                            Выйти
                        </button>
                    </ul>
                </aside>
                <main className="col-start-3 col-span-full max-h-screen py-8">
                    <div className="w-full  rounded-lg bg-white p-8 max-h-full overflow-y-scroll">
                        {page}
                    </div>
                </main>
            </section>
        </div>
    );
};
export default getAdminLayout;
