import { ApolloClient, InMemoryCache } from "@apollo/client";

const client = new ApolloClient({
    // IMPORTANT!: ApolloClint uri
    uri: "http://94.247.128.224:3000/graphql",
    cache: new InMemoryCache(),
});

export default client;
