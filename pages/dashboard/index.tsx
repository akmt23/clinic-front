import getAdminLayout from "components/layouts/adminLayout";
import { NextPage } from "next";
import React from "react";

const DashboardPage = () => {
    return <div>helloWorld</div>;
};

DashboardPage.getLayout = (page: NextPage) => getAdminLayout(page);

export default DashboardPage;
