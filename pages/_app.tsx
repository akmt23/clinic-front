import React, { ReactElement, ReactNode } from "react";
import { AppProps } from "next/app";
import "tailwindcss/tailwind.css";
import "public/styles/global.css";
import { NextPage } from "next";
import DependeciesWrapper from "src/utils/DependeciesWrapper";
import { SessionProvider } from "next-auth/react";
import withAuth from "components/hocs/withAuth";

type NextPageWithLayout = NextPage & {
    getLayout?: (page: ReactElement) => ReactNode;
};

type AppPropsWithLayout = AppProps & {
    Component: NextPageWithLayout;
};

function MyApp({
    Component,
    pageProps: { session, ...pageProps },
}: AppPropsWithLayout): JSX.Element {
    const getLayout = Component.getLayout || ((page) => page);
    return (
        <SessionProvider session={session}>
            <DependeciesWrapper>
                {getLayout(<Component {...pageProps} />)}
            </DependeciesWrapper>
        </SessionProvider>
    );
}

export default MyApp;
